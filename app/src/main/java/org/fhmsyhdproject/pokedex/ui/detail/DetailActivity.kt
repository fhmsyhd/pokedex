package org.fhmsyhdproject.pokedex.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import org.fhmsyhdproject.pokedex.R
import org.fhmsyhdproject.pokedex.core.data.source.Resource
import org.fhmsyhdproject.pokedex.core.domain.model.Pokemon
import org.fhmsyhdproject.pokedex.databinding.ActivityDetailBinding
import org.koin.android.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity() {

    private val detailViewModel: DetailViewModel by viewModel()

    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.hpProgressBar.setProgress(68)
        binding.atkProgressBar.setProgress(87)

        val detailPokemon = intent.getParcelableExtra<Pokemon>(EXTRA_DATA)

        if (detailPokemon != null) {
            detailViewModel.requestPokemonInfo(detailPokemon.name).observe(this, { pokemonInfo ->
                if (pokemonInfo != null){
                    when(pokemonInfo){
                        is Resource.Success -> {
                            pokemonInfo.data?.map {
                                binding.tvName.text = it.name
                                binding.tvWeight.text = it.weight.toString()
                                binding.tvHeight.text = it.height.toString()
                            }

                        }
                        is Resource.Error -> {
                            binding.tvName.text = "Unloaded"
                        }
                    }
                }

            })
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}