package org.fhmsyhdproject.pokedex.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import org.fhmsyhdproject.pokedex.core.data.source.Resource
import org.fhmsyhdproject.pokedex.core.ui.PokemonAdapter
import org.fhmsyhdproject.pokedex.databinding.ActivityHomeBinding
import org.fhmsyhdproject.pokedex.ui.detail.DetailActivity
import org.koin.android.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    private val homeViewModel: HomeViewModel by viewModel()

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val pokemonAdapter = PokemonAdapter()
        pokemonAdapter.onItemClick = { selectedData ->
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra(DetailActivity.EXTRA_DATA, selectedData)
            startActivity(intent)
        }

        homeViewModel.pokemon.observe(this, { pokemon ->
            if (pokemon != null) {
                when (pokemon){
                    is Resource.Loading -> binding.loadingAnimation.visibility = View.VISIBLE
                    is Resource.Success -> {
                        binding.loadingAnimation.visibility = View.GONE
                        pokemonAdapter.setData(pokemon.data)
                    }
                    is Resource.Error -> {
                        binding.loadingAnimation.visibility = View.GONE
                        binding.viewError.root.visibility = View.VISIBLE
                    }
                }
            }
        })

        with(binding.rvPokemon) {
            layoutManager = GridLayoutManager(context, 2)
            setHasFixedSize(true)
            adapter = pokemonAdapter
        }
    }
}