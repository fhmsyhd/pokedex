package org.fhmsyhdproject.pokedex.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import org.fhmsyhdproject.pokedex.core.data.source.Resource
import org.fhmsyhdproject.pokedex.core.domain.model.PokemonDetail
import org.fhmsyhdproject.pokedex.core.domain.usecase.PokemonUseCase

class DetailViewModel(private val pokemonUseCase: PokemonUseCase): ViewModel() {

    fun requestPokemonInfo(name: String): LiveData<Resource<List<PokemonDetail>>> {
        return pokemonUseCase.getPokemonInfo(name).asLiveData()
    }

}