package org.fhmsyhdproject.pokedex.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import org.fhmsyhdproject.pokedex.core.domain.usecase.PokemonUseCase

class HomeViewModel(pokemonUseCase: PokemonUseCase): ViewModel() {
    val pokemon = pokemonUseCase.getAllPokemon().asLiveData()
}