package org.fhmsyhdproject.pokedex.di

import org.fhmsyhdproject.pokedex.core.domain.usecase.PokemonInteractor
import org.fhmsyhdproject.pokedex.core.domain.usecase.PokemonUseCase
import org.fhmsyhdproject.pokedex.ui.detail.DetailViewModel
import org.fhmsyhdproject.pokedex.ui.home.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<PokemonUseCase> { PokemonInteractor(get()) }
}

val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { DetailViewModel(get()) }
}