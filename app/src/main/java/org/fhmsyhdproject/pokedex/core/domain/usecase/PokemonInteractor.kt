package org.fhmsyhdproject.pokedex.core.domain.usecase

import org.fhmsyhdproject.pokedex.core.domain.repository.IPokemonRepository

class PokemonInteractor(private val pokemonRepository: IPokemonRepository): PokemonUseCase {

    override fun getAllPokemon() = pokemonRepository.getAllPokemon()

    override fun getPokemonInfo(name: String) = pokemonRepository.getPokemonInfo(name)
}