package org.fhmsyhdproject.pokedex.core.data.source.remote

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.fhmsyhdproject.pokedex.core.data.source.remote.network.ApiResponse
import org.fhmsyhdproject.pokedex.core.data.source.remote.network.ApiService
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonDetailResponse
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonResponse

class RemoteDataSource(private val apiService: ApiService) {

    suspend fun getAllPokemon(): Flow<ApiResponse<List<PokemonResponse>>> {
        return flow {
            try {
                val response = apiService.getListPokemon()
                val dataArray = response.results
                if (dataArray.isNotEmpty()){
                    emit(ApiResponse.Success(response.results))
                } else {
                    emit(ApiResponse.Empty)
                }
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getPokemonInfo(name: String): Flow<ApiResponse<List<PokemonDetailResponse>>> {
        
        return flow {
            try {
                val response = apiService.getPokemonInfo(name)
                val dataArray = response
                emit(ApiResponse.Success(response))
            } catch (e: Exception) {
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }
}