package org.fhmsyhdproject.pokedex.core.data.source.local.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemonDetail")
data class PokemonDetailEntity (
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "pokemonId")
        var pokemonId: Int? = 0,

        @ColumnInfo(name = "name")
        var name: String? = "",

        @ColumnInfo(name = "height")
        var height: Int? = 0,

        @ColumnInfo(name = "weight")
        var weight: Int? = 0,

//        @ColumnInfo(name = "image")
//        var image: String? = "",

//        @ColumnInfo(name = "hp")
//        var hp: Int? = 0,
//
//        @ColumnInfo(name = "attack")
//        var attack: Int? = 0,
//
//        @ColumnInfo(name = "defense")
//        var defense: Int? = 0,
//
//        @ColumnInfo(name = "specialAttack")
//        var specialAttack: Int? = 0,
//
//        @ColumnInfo(name = "specialDefense")
//        var specialDefense: Int? = 0,
//
//        @ColumnInfo(name = "speed")
//        var speed: Int? = 0,
)