package org.fhmsyhdproject.pokedex.core.data.source.local

import kotlinx.coroutines.flow.Flow
import org.fhmsyhdproject.pokedex.core.data.source.local.entity.PokemonDetailEntity
import org.fhmsyhdproject.pokedex.core.data.source.local.entity.PokemonEntity
import org.fhmsyhdproject.pokedex.core.data.source.local.room.PokemonDao

class LocalDataSource(private val pokemonDao: PokemonDao) {

    fun getAllPokemon(): Flow<List<PokemonEntity>> = pokemonDao.getAllPokemon()

    suspend fun insertPokemon(pokemonList: List<PokemonEntity>) = pokemonDao.insertPokemon(pokemonList)

    fun getPokemonInfo(): Flow<List<PokemonDetailEntity>> = pokemonDao.getPokemonInfo()

    suspend fun insertPokemonInfo(pokemonInfoList: List<PokemonDetailEntity>) = pokemonDao.insertPokemonInfo(pokemonInfoList)
}