package org.fhmsyhdproject.pokedex.core.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.fhmsyhdproject.pokedex.R
import org.fhmsyhdproject.pokedex.core.domain.model.Pokemon
import org.fhmsyhdproject.pokedex.databinding.ItemPokemonBinding


class PokemonAdapter: RecyclerView.Adapter<PokemonAdapter.ListViewHolder>() {

    private var listData = ArrayList<Pokemon>()
    var onItemClick: ((Pokemon) -> Unit)? = null

    fun setData(newListData: List<Pokemon>?) {
        if (newListData == null) return
        listData.clear()
        listData.addAll(newListData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ) = ListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false))

    override fun onBindViewHolder(holder: PokemonAdapter.ListViewHolder, position: Int) {
        val data = listData[position]
        holder.bind(data)
    }

    override fun getItemCount(): Int = listData.size

    inner class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = ItemPokemonBinding.bind(itemView)
        fun bind(data: Pokemon) {
            with(binding) {
                val upperString: String = data.name.substring(0, 1).toUpperCase() + data.name.substring(1).toLowerCase()
                tvPokemonName.text = upperString
            }
        }

        init {
            binding.root.setOnClickListener {
                onItemClick?.invoke(listData[adapterPosition])
            }
        }
    }
}