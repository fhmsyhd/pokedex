package org.fhmsyhdproject.pokedex.core.data.source

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.fhmsyhdproject.pokedex.core.data.source.local.LocalDataSource
import org.fhmsyhdproject.pokedex.core.data.source.remote.RemoteDataSource
import org.fhmsyhdproject.pokedex.core.data.source.remote.network.ApiResponse
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonDetailResponse
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonResponse
import org.fhmsyhdproject.pokedex.core.domain.model.Pokemon
import org.fhmsyhdproject.pokedex.core.domain.model.PokemonDetail
import org.fhmsyhdproject.pokedex.core.domain.repository.IPokemonRepository
import org.fhmsyhdproject.pokedex.core.utils.AppExecutors
import org.fhmsyhdproject.pokedex.core.utils.DataMapper
import kotlin.math.log

class PokemonRepository(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource,
    private val appExecutors: AppExecutors
): IPokemonRepository {

    override fun getAllPokemon(): Flow<Resource<List<Pokemon>>> =
        object : NetworkBoundResource<List<Pokemon>, List<PokemonResponse>>() {
            override fun loadFromDB(): Flow<List<Pokemon>> {
                return localDataSource.getAllPokemon().map {
                    DataMapper.mapEntitiesToDomain(it)
                }
            }

            override fun shouldFetch(data: List<Pokemon>?): Boolean = true

            override suspend fun createCall(): Flow<ApiResponse<List<PokemonResponse>>> =
                remoteDataSource.getAllPokemon()

            override suspend fun saveCallResult(data: List<PokemonResponse>) {
                val pokemonList = DataMapper.mapResponsesToEntities(data)
                localDataSource.insertPokemon(pokemonList)
            }

        }.asFlow()

    override fun getPokemonInfo(name: String): Flow<Resource<List<PokemonDetail>>> =
            object : NetworkBoundResource<List<PokemonDetail>, List<PokemonDetailResponse>>() {
                override fun loadFromDB(): Flow<List<PokemonDetail>> {
                    return localDataSource.getPokemonInfo().map {
                        DataMapper.mapDetailEntitiesToDomain(it)
                    }
                }

                override fun shouldFetch(data: List<PokemonDetail>?): Boolean = true

                override suspend fun createCall(): Flow<ApiResponse<List<PokemonDetailResponse>>> =
                        remoteDataSource.getPokemonInfo(name)

                override suspend fun saveCallResult(data: List<PokemonDetailResponse>) {
                    val pokemonList = DataMapper.mapDetailResponsesToEntities(data)
                    localDataSource.insertPokemonInfo(pokemonList)
                }

            }.asFlow()
}