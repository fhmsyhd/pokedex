package org.fhmsyhdproject.pokedex.core.domain.repository

import kotlinx.coroutines.flow.Flow
import org.fhmsyhdproject.pokedex.core.data.source.Resource
import org.fhmsyhdproject.pokedex.core.domain.model.Pokemon
import org.fhmsyhdproject.pokedex.core.domain.model.PokemonDetail

interface IPokemonRepository {

    fun getAllPokemon(): Flow<Resource<List<Pokemon>>>

    fun getPokemonInfo(name: String): Flow<Resource<List<PokemonDetail>>>

}