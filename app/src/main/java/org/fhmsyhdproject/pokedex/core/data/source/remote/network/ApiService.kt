package org.fhmsyhdproject.pokedex.core.data.source.remote.network

import org.fhmsyhdproject.pokedex.core.data.source.remote.response.ListPokemonResponse
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonDetailResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {
    @GET("pokemon")
    suspend fun getListPokemon(
//        @Query("limit") limit: Int,
//        @Query("offset") offset: Int
    ): ListPokemonResponse

    @GET("pokemon/{name}")
    suspend fun getPokemonInfo(
        @Path("name") name: String
    ): List<PokemonDetailResponse>
}