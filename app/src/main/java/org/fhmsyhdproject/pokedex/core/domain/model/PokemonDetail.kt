package org.fhmsyhdproject.pokedex.core.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PokemonDetail (
        val pokemonId: Int,
        val name: String,
        val height: Int,
        val weight: Int,
//        val image: String,
//        val hp: Int,
//        val attack: Int,
//        val defense: Int,
//        val specialAttack: Int,
//        val specialDefense: Int,
//        val speed: Int
) : Parcelable