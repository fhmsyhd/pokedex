package org.fhmsyhdproject.pokedex.core.utils

import org.fhmsyhdproject.pokedex.core.data.source.local.entity.PokemonDetailEntity
import org.fhmsyhdproject.pokedex.core.data.source.local.entity.PokemonEntity
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonDetailResponse
import org.fhmsyhdproject.pokedex.core.data.source.remote.response.PokemonResponse
import org.fhmsyhdproject.pokedex.core.domain.model.Pokemon
import org.fhmsyhdproject.pokedex.core.domain.model.PokemonDetail

object DataMapper {

    fun mapResponsesToEntities(input: List<PokemonResponse>): List<PokemonEntity> {
        val pokemonList = ArrayList<PokemonEntity>()
        input.map {
            val pokemon = PokemonEntity(
                name = it.name,
                url = it.url
            )
            pokemonList.add(pokemon)
        }
        return pokemonList
    }

    fun mapEntitiesToDomain(input: List<PokemonEntity>): List<Pokemon> =
        input.map {
            Pokemon(
                name = it.name,
                url = it.url
            )
        }

    fun mapDomainToEntity(input: Pokemon) = PokemonEntity(
        name = input.name,
        url = input.url
    )

    fun mapDetailResponsesToEntities(input: List<PokemonDetailResponse>): List<PokemonDetailEntity> {
        val pokemonList = ArrayList<PokemonDetailEntity>()
        input.map {
            val pokemon = PokemonDetailEntity(
                    pokemonId = it.id,
                    name = it.name,
                    height = it.height,
                    weight = it.weight,
//                    image = it.sprites.frontDefault,
//                    hp = it.stats[0].baseStat,
//                    attack = it.stats[1].baseStat,
//                    defense = it.stats[2].baseStat,
//                    specialAttack = it.stats[3].baseStat,
//                    specialDefense = it.stats[4].baseStat,
//                    speed = it.stats[5].baseStat
            )
            pokemonList.add(pokemon)
        }
        return pokemonList
    }

    fun mapDetailEntitiesToDomain(input: List<PokemonDetailEntity>): List<PokemonDetail> =
        input.map {
            PokemonDetail(
                    pokemonId = it.pokemonId!!,
                    name = it.name!!,
                    height = it.height!!,
                    weight = it.weight!!,
//                    image = it.image!!,
//                    hp = it.hp!!,
//                    attack = it.attack!!,
//                    defense = it.defense!!,
//                    specialAttack = it.specialAttack!!,
//                    specialDefense = it.specialDefense!!,
//                    speed = it.speed!!
            )
        }

    fun mapDetailDomainToEntity(input: PokemonDetail) = PokemonDetailEntity(
            pokemonId = input.pokemonId,
            name = input.name,
            height = input.height,
            weight = input.weight,
//            image = input.image,
//            hp = input.hp,
//            attack = input.attack,
//            defense = input.defense,
//            specialAttack = input.specialAttack,
//            specialDefense = input.specialDefense,
//            speed = input.speed
    )
}