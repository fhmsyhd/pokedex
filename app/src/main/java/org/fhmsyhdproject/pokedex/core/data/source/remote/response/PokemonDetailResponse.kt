package org.fhmsyhdproject.pokedex.core.data.source.remote.response

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class PokemonDetailResponse (

    @field:SerializedName("id")
    val id: Int,

    @field:SerializedName("name")
    val name: String,

    @field:SerializedName("height")
    val height: Int,

    @field:SerializedName("weight")
    val weight: Int,

//    @field:SerializedName("stats")
//    val stats: List<StatsItem>,

//    @field:SerializedName("sprites")
//    val sprites: Sprites

//    @field:SerializedName("types")
//    val types: TypesItem
)


//data class Sprites(
//
//        @field:SerializedName("back_default")
//        val backDefault: String,
//
//        @field:SerializedName("front_shiny_female")
//        val frontShinyFemale: Any,
//
//        @field:SerializedName("front_default")
//        val frontDefault: String,
//
//        @field:SerializedName("front_female")
//        val frontFemale: Any,
//
//        @field:SerializedName("front_shiny")
//        val frontShiny: String
//)

//data class StatsItem(
//
//        @field:SerializedName("stat")
//        val stat: Stat,
//
//        @field:SerializedName("base_stat")
//        val baseStat: Int,
//
//        @field:SerializedName("effort")
//        val effort: Int
//)
//
//data class Stat(
//
//        @field:SerializedName("name")
//        val name: String,
//
//        @field:SerializedName("url")
//        val url: String
//)

//data class TypesItem(
//
//        @field:SerializedName("slot")
//        val slot: Int,
//
//        @field:SerializedName("type")
//        val type: Type
//)
//
//data class Type(
//
//        @field:SerializedName("name")
//        val name: String,
//
//        @field:SerializedName("url")
//        val url: String
//)