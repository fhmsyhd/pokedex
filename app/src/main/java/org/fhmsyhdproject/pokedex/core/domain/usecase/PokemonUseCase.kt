package org.fhmsyhdproject.pokedex.core.domain.usecase

import kotlinx.coroutines.flow.Flow
import org.fhmsyhdproject.pokedex.core.data.source.Resource
import org.fhmsyhdproject.pokedex.core.domain.model.Pokemon
import org.fhmsyhdproject.pokedex.core.domain.model.PokemonDetail

interface PokemonUseCase {

    fun getAllPokemon(): Flow<Resource<List<Pokemon>>>

    fun getPokemonInfo(name: String): Flow<Resource<List<PokemonDetail>>>
}