package org.fhmsyhdproject.pokedex

import android.app.Application
import org.fhmsyhdproject.pokedex.core.di.databaseModule
import org.fhmsyhdproject.pokedex.core.di.networkModule
import org.fhmsyhdproject.pokedex.core.di.repositoryModule
import org.fhmsyhdproject.pokedex.di.useCaseModule
import org.fhmsyhdproject.pokedex.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@MyApplication)
            modules(
                listOf(
                    databaseModule,
                    networkModule,
                    repositoryModule,
                    useCaseModule,
                    viewModelModule
                )
            )
        }
    }
}